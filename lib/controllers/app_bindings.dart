import 'package:get/get.dart';
import 'package:kia_mobile/controllers/app_controller.dart';
import 'package:kia_mobile/controllers/auth_controller.dart';
import 'package:kia_mobile/controllers/ibuhamil_controller.dart';

class AppBindings implements Bindings {
  @override
  void dependencies() {
    Get.put<AuthController>(AuthController(), permanent: true);
    Get.put<AppController>(AppController(), permanent: true);
    Get.put<IbuhamilController>(IbuhamilController(), permanent: true);
  }
}
