import 'package:get/get.dart';
import 'package:kia_mobile/controllers/auth_controller.dart';
import 'package:kia_mobile/models/app_model.dart';

class AppController extends GetxController {
  Rx<App> app =
      App(isLoading: true, appName: 'Pos', deviceId: '', isAuth: false).obs;

  @override
  void onInit() {
    super.onInit();
    appInit();
    Get.find<AuthController>().isAuth.listen((isAuth) {
      app(app.value.copyWith(isAuth: isAuth));
    });
  }

  Future<void> appInit() async {
    print('App Init -------');
    // await Future.delayed(Duration(seconds: 2));
    ever(app, goToLandingPage);
    // app(app.value.copyWith(isLoading: false));
  }

  void goToLandingPage(App appStatus) {
    print('Go To landing Page');
    final auth = appStatus.isAuth;
    if (auth != null) {
      if (auth) {
        Get.offAllNamed('/home');
      } else {
        Get.offAllNamed('/login');
      }
      app(app.value.copyWith(isLoading: false));
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
