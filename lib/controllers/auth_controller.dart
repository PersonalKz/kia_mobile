import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:kia_mobile/models/auth_model.dart';

class AuthController extends GetxController {
  final box = GetStorage();
  Rx<AuthModel> authModel = AuthModel(accessToken: "", tokenType: "").obs;
  RxBool isAuth = false.obs;

  @override
  void onInit() {
    _handleAuth();
    super.onInit();
  }

  static String baseUrl = "http://10.0.2.2:1818/api";
  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code! >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<AuthModel> login(String username, String password) async {
    try {
      var response = await dio.post('/auth/signin',
          data: {"usernameOrEmail": username, "password": password});
      var result = json.decode(response.data);

      if (result['accessToken'] != null) {
        AuthModel auth = AuthModel.fromMap(result);
        _persistToken('token', auth);
        print("cek");

        authModel.value = auth;

        // Get.offAllNamed('/home');
      } else {
        Get.snackbar(
          "404",
          "password salah",
          snackPosition: SnackPosition.TOP,
        );
      }

      return authModel.value;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        Get.snackbar(
          "404",
          "Internet not connect",
          snackPosition: SnackPosition.TOP,
        );
      }
      throw Exception(e.message);
    }
  }

  Future<void> _persistToken(String key, value) async {
    await box.write(key, json.encode(value));
  }

  Future<void> deleteToken() async {
    await box.remove('token');
  }

  Future<dynamic> getToken() async {
    var result = json.decode(box.read('token'));

    print("cek");
    return result;
  }

  Future<bool> _hasToken() async {
    final String token = box.read('token') ?? '';
    if (token != '') {
      return true;
    } else {
      return false;
    }
  }

  _handleAuth() async {
    if (await _hasToken()) {
      var res = AuthModel.fromJson(await getToken());
      print("ce");
      isAuth.value = true;
      authModel.value = res;
    } else {
      AuthModel(accessToken: "", tokenType: "");
      isAuth.value = false;
    }
  }
}
