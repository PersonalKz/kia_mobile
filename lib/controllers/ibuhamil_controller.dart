import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:kia_mobile/models/auth_model.dart';
import 'package:kia_mobile/models/ibuhamil_model.dart';

class IbuhamilController extends GetxController {
  RxList<Ibuhamil> ibuhamil = List<Ibuhamil>.empty().obs;
  String token = "";
  var box = GetStorage();

  @override
  void onInit() {
    var res = AuthModel.fromJson(json.decode(box.read('token')));
    token = res.accessToken;
    super.onInit();
  }

  @override
  void onReady() {
    print("cek");
    super.onReady();
    getibuhamil(token);
  }

  static String baseUrl = "http://10.0.2.2:1818";
  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code! >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);
  Future<List<Ibuhamil>> getibuhamil(String token) async {
    try {
      var response = await dio.get('/api/ibu',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer ' + token}));
      print('cek');
      List result = json.decode(response.data);
      List<Ibuhamil> ibuhamilData =
          result.map((pd) => Ibuhamil.fromMap(pd)).toList();
      ibuhamil.value = ibuhamilData;
      print('cek');

      return ibuhamilData;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        Get.snackbar(
          "404",
          "Internet not connect",
          snackPosition: SnackPosition.TOP,
        );
      }
      throw Exception(e.message);
    }
  }
}
