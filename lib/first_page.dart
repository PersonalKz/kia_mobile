import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kia_mobile/controllers/auth_controller.dart';
import 'package:kia_mobile/views/home_page.dart';
import 'package:kia_mobile/views/login/login_page.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetX<AuthController>(
        init: Get.put<AuthController>(AuthController()),
        builder: (AuthController? authController) {
          if (authController != null) {
            return HomePage();
          } else {
            return LoginPage();
          }
        });
  }
}
