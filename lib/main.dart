import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:kia_mobile/app_routes.dart';
import 'package:kia_mobile/controllers/app_bindings.dart';

import 'views/splash_page.dart';

void main() async {
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'KIA',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialBinding: AppBindings(),
      home: SplashPage(),
      getPages: AppRoutes.routes,
    );
  }
}
