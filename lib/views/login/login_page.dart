import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kia_mobile/constants.dart';
import 'package:kia_mobile/controllers/auth_controller.dart';
import 'package:kia_mobile/views/login/background.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String? userName;
  String? password;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    AuthController authController = Get.find<AuthController>();
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Image.asset(
                "assets/images/bumil.png",
                height: size.height * 0.35,
              ),
              SizedBox(height: size.height * 0.03),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: size.width * 0.8,
                decoration: BoxDecoration(
                  color: kPrimaryLightColor,
                  borderRadius: BorderRadius.circular(29),
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                      icon: Icon(Icons.person),
                      hintText: 'Email or Username',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      border: InputBorder.none),
                  validator: (val) {
                    if (val!.isEmpty) {
                      return 'Silahkan Isi Email atau Username Anda';
                    }
                  },
                  onSaved: (val) => userName = val,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: size.width * 0.8,
                decoration: BoxDecoration(
                  color: kPrimaryLightColor,
                  borderRadius: BorderRadius.circular(29),
                ),
                child: TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock),
                      hintText: 'Input Password',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      border: InputBorder.none),
                  validator: (val) {
                    if (val!.isEmpty) {
                      return 'Silahkan Isi Password Anda';
                    }
                  },
                  onSaved: (val) => password = val,
                ),
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(29))),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      print("cek");
                      authController.login(userName!, password!);
                    }
                  },
                  child: Text("Login"))
            ],
          ),
        ),
      ),
    );
  }
}
